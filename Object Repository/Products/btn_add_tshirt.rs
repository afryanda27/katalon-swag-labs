<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_add_tshirt</name>
   <tag></tag>
   <elementGuidId>dffe0b79-d2d0-483c-a865-66fb02c08abd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='add-to-cart-sauce-labs-bolt-t-shirt']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#add-to-cart-sauce-labs-bolt-t-shirt</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>be172e09-ec38-42ca-a629-825574b251ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn_primary btn_small btn_inventory</value>
      <webElementGuid>88ab6fbb-ae9e-4e99-85d6-04446f6b282e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-bolt-t-shirt</value>
      <webElementGuid>074683c5-b49b-4841-a33c-c628170339cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-bolt-t-shirt</value>
      <webElementGuid>e3d2696c-9d92-4a93-8c3a-fc91e7de6a97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-bolt-t-shirt</value>
      <webElementGuid>24673b3e-d882-4453-a0d3-34afec7a0e27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add to cart</value>
      <webElementGuid>c0dd65f3-58b7-4302-97de-38a665d84aa1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;add-to-cart-sauce-labs-bolt-t-shirt&quot;)</value>
      <webElementGuid>ac52763f-8e79-4a53-b804-a654a97c8c58</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='add-to-cart-sauce-labs-bolt-t-shirt']</value>
      <webElementGuid>20def2d9-106c-49f8-ad8f-ebb06b8fe305</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inventory_container']/div/div[3]/div[2]/div[2]/button</value>
      <webElementGuid>1214d1a0-a44d-460b-a8f4-0c432ad02ee7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$15.99'])[1]/following::button[1]</value>
      <webElementGuid>93badbd9-4ae6-4a77-b1ad-e73be21883a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sauce Labs Bolt T-Shirt'])[1]/following::button[1]</value>
      <webElementGuid>0b7393de-1dff-46ba-b764-c4b52d941ecc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sauce Labs Fleece Jacket'])[1]/preceding::button[1]</value>
      <webElementGuid>1e1ab0ee-bad3-4594-95f5-1de630698236</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$49.99'])[1]/preceding::button[1]</value>
      <webElementGuid>e3957867-d35d-4994-9aaf-f1ff2e2e03aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div[2]/button</value>
      <webElementGuid>3ad23f75-c29a-48bb-b551-3ae67b3faa61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'add-to-cart-sauce-labs-bolt-t-shirt' and @name = 'add-to-cart-sauce-labs-bolt-t-shirt' and (text() = 'Add to cart' or . = 'Add to cart')]</value>
      <webElementGuid>db5461ba-003b-4300-9d70-7fd8dad8cd94</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
