<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>dafe9577-1773-4d7f-9ac5-0558f0851fd5</testSuiteGuid>
   <testCaseLink>
      <guid>234b4ceb-7447-432f-be21-fb730116ff29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>01918372-c19d-45c7-bbe4-fbcf58245741</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Login Data</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>01918372-c19d-45c7-bbe4-fbcf58245741</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>8954e77e-1d3b-46cf-822c-b87ac4cc3292</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>01918372-c19d-45c7-bbe4-fbcf58245741</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>678fb49f-7428-4b5c-bc9e-eb0f9ab6cfe0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>01918372-c19d-45c7-bbe4-fbcf58245741</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>is_locked</value>
         <variableId>9b4694cf-fb6f-40dd-b131-b57d027deb3b</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
