<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS Login Error</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>cbe200e5-60fd-4906-92a8-2256b3e704b5</testSuiteGuid>
   <testCaseLink>
      <guid>cbd5961a-7f80-42ef-a8ef-e1df1ff23401</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC Login Error</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>6be1ac9b-52ea-4eb4-aa1a-436df49b31c0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Login Error Data</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>6be1ac9b-52ea-4eb4-aa1a-436df49b31c0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>9d6cf76f-8902-4248-8b4c-a889840528c1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6be1ac9b-52ea-4eb4-aa1a-436df49b31c0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>3a903e46-1e77-402f-b916-1f57fac52c5c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6be1ac9b-52ea-4eb4-aa1a-436df49b31c0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>message</value>
         <variableId>eac99380-7788-40fd-b030-1da5c8f675cc</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
